#!/bin/bash
CONFIG_FILE="/vagrant/creds/data.txt"
source $CONFIG_FILE

sudo mkdir -p /vagrant/backup && \
sudo -u postgres pg_dump -Fc $DB_NAME > /vagrant/db.dump && \
tar czvf /vagrant/project.tar.gz /vagrant/db.dump && \
openssl enc -aes-256-cbc -salt -in /vagrant/project.tar.gz -out /vagrant/backup/project.tar.gz.enc -k $TAR_PASS && \
sudo rm -rf /vagrant/project.tar.gz /vagrant/db.dump && \
echo "Dump export finished..."
