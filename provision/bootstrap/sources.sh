#!/usr/bin/env bash

echo "INFO - Updating and cleaning packages from apt repository..." && \
  sudo apt update -y && \
  sudo apt upgrade -y && \
  sudo sed -i "s/#\$nrconf{kernelhints} = -1;/\$nrconf{kernelhints} = -1;/g" /etc/needrestart/needrestart.conf && \
echo "INFO - Installing required sources..." && \
  sudo apt install -y build-essential checkinstall && \
  sudo apt install -y libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev libffi-dev zlib1g-dev && \
  sudo apt install -y libssl-dev libbz2-dev libffi-dev liblzma-dev lzma cron wget lvm2 software-properties-common net-tools && \
  cd /opt && \
  sudo wget https://www.python.org/ftp/python/3.8.12/Python-3.8.12.tgz && \
  sudo tar xzf Python-3.8.12.tgz && \
  cd Python-3.8.12 && \
  sudo ./configure --enable-optimizations && \
  sudo make && \
  sudo make altinstall && \
  cd /opt && \
  sudo rm -rf Python-3.8.12.tgz && \
  sudo apt -y install libc6 libpango-1.0-0 libpangocairo-1.0-0 zlib1g libpng16-16 libffi7 libpango1.0-dev libfreetype6-dev libfontconfig1-dev && \
echo "INFO - installation of postgres..." && \
  sudo apt install postgresql postgresql-contrib postgresql-client-common -y && \
  sudo adduser postgres && \
  sudo apt install -y git-lfs && \
  sudo git config --global user.name "project" && \
  sudo git config --global user.email "project@mail.com" && \
#echo 'export PATH=$PATH:/usr/pgsql-12/bin' >> ~/.bashrc &&\
echo "INFO - Sources installed successfully."