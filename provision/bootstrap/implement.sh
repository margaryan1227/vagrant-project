#!/usr/bin/env bash
CONFIG_FILE="/vagrant/creds/data.txt"
source $CONFIG_FILE

echo "INFO - Installing required images..." && \
  cd /srv/ && \
  sudo apt update -y && \
  sudo apt -y install unixodbc-dev lsof && \
  sudo apt -y install libudunits2-dev libcairo2-dev libpango1.0-dev && \
  sudo apt -y install poppler-utils language-pack-de && \
  sudo apt -y install ffmpeg libavcodec-dev && \
  sudo chmod -R 777 /srv/ && \
  sudo chown -R vagrant:vagrant /srv && \
  git config --global http.version HTTP/1.1 && \
  sudo apt-get install git-lfs && \
  if  [ ! -d "/srv/project" ]; then
    git clone $Git_URL $TARGET_DIR
  else
    git -C $TARGET_DIR pull
  fi && \
  cd $TARGET_DIR && \
  git config --global --add safe.directory $TARGET_DIR && \
  git lfs fetch && \
  git lfs fetch --all && \
  git lfs checkout && \
  git lfs pull && \
  sudo cp /vagrant/deployments/project.service /etc/systemd/system && \
  sudo cp /vagrant/creds/.env /srv/project/core/file_transver && \
  sudo systemctl restart postgresql@14-main.service && \
    sudo -u postgres psql -c "ALTER USER postgres PASSWORD '$DB_PASS';" && \
    if ! sudo -u postgres psql -lqt | cut -d \| -f 1 | grep -qw $DB_NAME ; then
      sudo -u postgres psql -c "CREATE DATABASE $DB_NAME;" && \
      if test -d /vagrant/backup && test -f /vagrant/backup/project.tar.gz.enc; then
        openssl enc -d -aes-256-cbc -in /vagrant/backup/project.tar.gz.enc -out /vagrant/project.tar.gz -k $TAR_PASS && \
        tar xzvf /vagrant/project.tar.gz --to-stdout > db.dump && \
        sudo -u postgres pg_restore -d $DB_NAME db.dump && \
        sudo rm -rf db.dump
      fi
    fi && \
    sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO postgres;" && \
  sudo mkdir -p /var/log/project && \
  sudo chmod -R 777 /var/log/project/ && \
  sudo chmod -R 777 /srv/ && \
  cd /vagrant/deployments/ && \
  sudo chmod +x *.sh && \
  cd /srv/project/core/ && \
  sudo apt -y install python3-pip && \
  /usr/local/bin/python3.8 -m pip install --upgrade pip && \
  /usr/local/bin/python3.8 -m pip install -r ../requirements.txt && \
  /usr/local/bin/python3.8 manage.py makemigrations && \
  /usr/local/bin/python3.8 manage.py migrate && \
  /usr/local/bin/python3.8 manage.py add_all && \
  /usr/local/bin/python3.8 manage.py $VERSION && \
  /usr/local/bin/python3.8 manage.py create_server_password --new && \
  git config --global --add safe.directory /srv/project && \
  sudo sed -E -i 's/(filemode\s+\=\s+)true/\1false/' /srv/project/.git/config && \
  sudo chmod -R 777 /srv/ && \
  sudo systemctl daemon-reload && \
  sudo systemctl enable project.service && \
  sudo systemctl start project.service && \
  # Install cron and set up backup job
  sudo touch /var/log/pg_backup.log && \
  sudo chmod 666 /var/log/pg_backup.log && \
  sudo mkdir -p /vagrant/backup && \
  sudo chmod 777 /vagrant/backup && \
  sudo chmod 777 /etc/crontab && \
  sudo mkdir -p /home/ubuntu/schedulers && \
  sudo sed -i 's/\r$//' /vagrant/provision/bootstrap/export-dump.sh && \
  sudo chmod +x /vagrant/provision/bootstrap/export-dump.sh && \
  sudo mv /vagrant/provision/bootstrap/export-dump.sh /home/ubuntu/schedulers/ && \
# PostgreSQL database dump script
  echo '0 * * * * sudo /home/ubuntu/schedulers/export-dump.sh' >> /etc/crontab && \
# Copy folder from A to B every hour
  echo '0 * * * * cp -R /srv/project/core/media /vagrant/backup' >> /etc/crontab && \
# service cron restart
  echo "INFO - project images installed successfully."