#!/usr/bin/env bash

IP=$(ifconfig enp0s8 | grep 'inet ' | awk '{print $2}')
CONFIG_FILE="/vagrant/creds/data.txt"
source $CONFIG_FILE

echo "INFO - Installing neccessary packages for NextCloud..." && \
    sudo apt update -y && \
    sudo apt install -y lsb-release ca-certificates apt-transport-https software-properties-common && \
    sudo apt install -y wget unzip && \
    sudo apt install -y php8.1 && \
    sudo apt install -y php8.1-{bcmath,xml,fpm,intl,ldap,gd,cli,bz2,curl,mbstring,pgsql,opcache,soap,cgi,zip} && \
    sudo apt install -y apache2 libapache2-mod-php8.1 avahi-daemon && \
    sudo systemctl enable avahi-daemon && \
    sudo systemctl start avahi-daemon && \
    sudo a2enmod php8.1 && \
    sudo systemctl restart apache2 && \
echo "INFO - Installing OpenSSL and Configure Apache for HTTPS..." && \
    sudo apt install openssl -y && \
    sudo mkdir -p /etc/apache2/certs && \
    cd /etc/apache2/certs/ && \
    sudo a2enmod ssl rewrite proxy proxy_http && \
    sudo systemctl restart apache2 && \
    sudo openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out apache.crt -keyout apache.key -subj "/CN=project" && \
    sudo cp /vagrant/deployments/project.conf /etc/apache2/sites-available/project.conf && \
    sudo a2dissite 000-default.conf && \
    sudo a2ensite project.conf && \
    sudo systemctl reload apache2 && \
    sudo systemctl restart apache2 && \
    sudo ufw enable && \
    sudo ufw allow https && \
    sudo ufw allow ssh && \
    sudo ufw allow 5432 && \
echo "INFO - Configure PostgreSQL..." && \
    sudo sed -i 's/scram-sha-256/md5/g' /etc/postgresql/14/main/pg_hba.conf && \
    sudo sed -i 's/ident/md5/g' /etc/postgresql/14/main/pg_hba.conf && \
    sudo sed -i 's|127.0.0.1/32|192.168.224.0/24|g' /etc/postgresql/14/main/pg_hba.conf && \
    sudo sed -i "s|#listen_addresses = 'localhost'|listen_addresses = '*'|g" /etc/postgresql/14/main/postgresql.conf && \
    sudo sed -i '$a\host    all             all        127.0.0.1/32            md5' /etc/postgresql/14/main/pg_hba.conf  &&\
    sudo systemctl enable postgresql@14-main.service && \
    sudo systemctl start postgresql@14-main.service && \
    sudo -u postgres psql -c "ALTER USER postgres PASSWORD '$DB_PASS';"
    if ! sudo -u postgres psql -lqt | cut -d \| -f 1 | grep -qw nextcloud; then
        sudo -u postgres psql -c "CREATE DATABASE nextcloud;"
    fi && \
    sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE nextcloud TO postgres;" && \
echo "INFO - Installing Nextcloud..." && \
    cd /tmp
    if ! [ -d "/var/www/nextcloud" ]; then
        wget https://download.nextcloud.com/server/releases/latest.zip && \
        unzip latest.zip && \
        sudo rm -rf latest.zip && \
        sudo mv nextcloud/ /var/www
    fi && \
    sudo chown -R www-data:www-data /var/www/nextcloud/ && \
    sudo cp /vagrant/deployments/nc.conf /etc/apache2/conf-enabled/nextcloud.conf && \
    sudo a2enmod rewrite dir mime env headers && \
    sudo systemctl restart apache2 && \
    sudo mkdir -p /var/www/nextcloud/data && \
    sudo chown -R www-data:www-data /var/www/nextcloud/data && \
echo "INFO - Configure Nextcloud..." && \
    sudo -u www-data php /var/www/nextcloud/occ maintenance:install \
        --database pgsql --database-name nextcloud \
        --database-user postgres --database-pass $DB_PASS \
        --admin-user $NC_USER --admin-pass $NC_PASS \
        --data-dir /var/www/nextcloud/data --database-host localhost && \
    sudo sed -i "s|0 => 'localhost'|0 => 'localhost', 1 => '$IP'|g" /var/www/nextcloud/config/config.php && \
    sudo systemctl restart apache2 && \
    sudo -u www-data php /var/www/nextcloud/occ app:install calendar && \
    sudo -u www-data php /var/www/nextcloud/occ theming:config name $NC_NAME && \
    sudo -u www-data php /var/www/nextcloud/occ theming:config logo /vagrant/creds/logo.jpeg && \
    sudo -u www-data php /var/www/nextcloud/occ theming:config background /vagrant/creds/background.jpg && \
    sudo -u www-data php /var/www/nextcloud/occ theming:config favicon /vagrant/creds/icon.png && \
    sudo -u www-data php /var/www/nextcloud/occ group:add $NC_USER_GROUP && \
    export OC_PASS=$NC_BASIC_USER_PASS && \
    sudo -u www-data -E php /var/www/nextcloud/occ user:add --password-from-env --group=$NC_USER_GROUP $NC_BASIC_USER && \
    export OC_PASS=$NC_BASIC_USER_PASS_2 && \
    sudo -u www-data -E php /var/www/nextcloud/occ user:add --password-from-env --group=$NC_USER_GROUP $NC_BASIC_USER_2 && \
echo "INFO - NextCloud configured..."
