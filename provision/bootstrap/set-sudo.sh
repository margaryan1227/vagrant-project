#!/usr/bin/env bash
CONFIG_FILE="/vagrant/creds/data.txt"
source $CONFIG_FILE

echo -e "$SUDO_PASS\n$SUDO_PASS" | sudo passwd root && \
echo -e "$SUDO_PASS\n$SUDO_PASS" | sudo passwd vagrant && \
sudo cp -r /vagrant/creds/sudoers /etc/sudoers && \
echo -e "$SUDO_PASS" | sudo reboot