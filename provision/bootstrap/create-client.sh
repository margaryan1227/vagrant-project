#!/bin/bash

# Generate a client certificate and key
if [ $# -ne 1 ]; then
   echo "Usage: $0 <client_name>"
   exit 1
fi

CLIENT_NAME=$1

cd /etc/openvpn/easy-rsa/ || exit
./easyrsa build-client-full $CLIENT_NAME nopass

# Generate client configuration file
cat > /etc/openvpn/client/$CLIENT_NAME.ovpn << EOF
client
dev tun
proto udp
remote $(curl -s https://ipinfo.io/ip) 1194
resolv-retry infinite
nobind
persist-key
persist-tun
comp-lzo
verb 3
<ca>
$(cat /etc/openvpn/easy-rsa/pki/ca.crt)
</ca>
<cert>
$(cat /etc/openvpn/easy-rsa/pki/issued/$CLIENT_NAME.crt)
</cert>
<key>
$(cat /etc/openvpn/easy-rsa/pki/private/$CLIENT_NAME.key)
</key>
EOF

# Set permissions on client configuration file
chmod 600 /etc/openvpn/client/$CLIENT_NAME.ovpn