#!/bin/bash

echo "Updating repository and installing necccessary packages..." && \
sudo yum update -y && \
sudo yum install -y epel-release && \
sudo yum update -y && \
sudo yum install -y openvpn wget zip easy-rsa && \
echo "Configuring OpenVPN server..." && \
sudo mkdir -p /etc/openvpn/easy-rsa && \
sudo cp -r /usr/share/easy-rsa/3.0.8/* /etc/openvpn/easy-rsa/ && \
sudo chown vagrant /etc/openvpn/easy-rsa && \
sudo cp /usr/share/doc/openvpn-2.4.*/sample/sample-config-files/server.conf /etc/openvpn && \
sudo sed -i 's|;push "redirect-gateway def1 bypass-dhcp"|push "redirect-gateway def1 bypass-dhcp"|g' /etc/openvpn/server.conf && \
sudo sed -i 's|;push "dhcp-option DNS 208.67.222.222sudo firewall-cmd --reload"|push "dhcp-option DNS 8.8.8.8"|g' /etc/openvpn/server.conf && \
sudo sed -i 's|;push "dhcp-option DNS 208.67.220.220"|push "dhcp-option DNS 8.8.4.4"|g' /etc/openvpn/server.conf && \
sudo sed -i 's|;user nobody|user nobody|g' /etc/openvpn/server.conf && \
sudo sed -i 's|;group nobody|group nobody|g' /etc/openvpn/server.conf && \
sudo sed -i 's|dh dh2048.pem|dh dh.pem|g' /etc/openvpn/server.conf && \
sudo sed -i 's|tls-auth ta.key 0 # This file is secret|;tls-auth ta.key 0 # This file is secret|g' /etc/openvpn/server.conf && \
sudo mkdir -p /etc/openvpn/easy-rsa/keys && \
cd /etc/openvpn/easy-rsa && \
./easyrsa init-pki && \
./easyrsa build-ca nopass && \
./easyrsa build-server-full server nopass && \
./easyrsa gen-dh && \
sudo cp /etc/openvpn/easy-rsa/pki/dh.pem /etc/openvpn/easy-rsa/pki/ca.crt /etc/openvpn/easy-rsa/pki/issued/server.crt /etc/openvpn/easy-rsa/pki/private/server.key /etc/openvpn && \
cd /etc/openvpn/easy-rsa && \
echo "Configuring Firewall and starting OpenVPN server..." && \
sudo systemctl start firewalld && \
sudo firewall-cmd --zone=trusted --add-service openvpn && \
sudo firewall-cmd --zone=trusted --add-service openvpn --permanent && \
sudo firewall-cmd --permanent --add-port=1194/udp && \
sudo firewall-cmd --list-services --zone=trusted && \
sudo firewall-cmd --add-masquerade && \
sudo firewall-cmd --permanent --add-masquerade && \
SHARK=$(ip route get 8.8.8.8 | awk 'NR==1 {print $(NF-2)}') && \
sudo firewall-cmd --permanent --direct --passthrough ipv4 -t nat -A POSTROUTING -s 10.8.0.0/24 -o $SHARK -j MASQUERADE && \
sudo firewall-cmd --reload && \
sudo systemctl -f enable openvpn@server.service && \
sudo systemctl start openvpn@server.service && \
echo "Creating client..." && \
cd /etc/openvpn/ && \
sudo chmod +x /vagrant/provision/bootstrap/create-client.sh && \
sudo /vagrant/provision/bootstrap/create-client.sh client && \
sudo cp /etc/openvpn/client/client.ovpn /vagrant/ && \
echo "OpenVPN installing finished."