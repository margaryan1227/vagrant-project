# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.define :ProjectVM do |ubuntu|
    ubuntu.vm.box = "ubuntu/jammy64"
    ubuntu.vm.box_version = "20230424.0.0"
    ubuntu.vm.network :public_network, ip: "192.168.224.155"
    ubuntu.vm.host_name = "project"
    ubuntu.vbguest.auto_update = false
    ubuntu.vm.provider "virtualbox" do |vb|
      vb.name = "ProjectVM"
      # Setting GUI off
      vb.gui = false
      # 6 Gb RAM
      vb.memory = "6144"
      # Setting CPU count
      vb.cpus = 2
    end
    # Setting SSH key config
    ubuntu.ssh.insert_key = false
    # Setting sync folder
    ubuntu.vm.synced_folder ".", "/vagrant", type: "virtualbox"
    # Setting up port forwarding
    ubuntu.vm.network "forwarded_port", guest: 80, host: 8000
    ubuntu.vm.network "forwarded_port", guest: 443, host: 4430
    ubuntu.vm.network "forwarded_port", guest: 5432, host: 5436
    # Running init script
    ubuntu.vm.provision "init", type: "shell", run: "always", inline: $init_ubuntu_provisioner
    # Running bootstrap scripts
    ubuntu.vm.provision "bootstrap", type: "shell", inline: $bootstrap_ubuntu_provisioner
    ubuntu.vm.provision "implement", type: "shell", inline: $implement_provisioner
    ubuntu.vm.provision "nextcloud", type: "shell", inline: $nextcloud_provisioner
  end
  config.vm.define :OpenVPNVM do |centos|
    vm_disk = 'CentOS.vdi'
    centos.vm.box = "centos/7"
    centos.vm.network :public_network, ip: "192.168.224.55"
    centos.vm.host_name = "openvpnvm"
    centos.vm.provider 'virtualbox' do |v|
      # 2 Gb RAM
      v.memory = 2048
      # Setting CPU count
      v.cpus = 1
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      unless File.exist?(vm_disk)
      v.customize ['createhd', '--filename', vm_disk, '--size', 40 * 1024]
      end
      # The controller name varies from OS to OS. If 'IDE' does not work try 'IDE Controller'
      v.customize ['storageattach', :id, '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', vm_disk]
    end
    # Setting SSH key config
    centos.ssh.insert_key = false
    # Setting sync folder
    centos.vm.synced_folder ".", "/vagrant", type: "virtualbox"
    # Running init script
    centos.vm.provision "init", type: "shell", run: "always", inline: $init_centos_provisioner
    # Running bootstrap script
    centos.vm.provision "bootstrap", type: "shell", inline: $bootstrap_centos_provisioner
    # Setting up port forwarding
    centos.vm.network :forwarded_port, guest: 1194, host: 11194, guest_ip: "192.168.224.55", host_ip: "127.0.0.1"
  end
end

$init_ubuntu_provisioner = <<-SCRIPT
sudo sysctl -w net.ipv4.ip_forward=1
sed -i 's/\r$//' /vagrant/creds/data.txt
sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/ssh_config
sudo service ssh restart
sudo apt-get update -y
sudo apt-get install dos2unix -y
sudo apt-get clean -y
echo "nameserver 8.8.8.8" | sudo tee -a /etc/resolv.conf
SCRIPT

$bootstrap_ubuntu_provisioner = <<-SCRIPT
DIR="/vagrant/provision/bootstrap"
echo "INFO - Starting core provisioning..."
sed -i 's/\r$//' $DIR/sources.sh
/usr/bin/env bash "$DIR/sources.sh"
echo "INFO - project provisioning completed successfully!"
SCRIPT

$implement_provisioner = <<-SCRIPT
DIR="/vagrant/provision/bootstrap"
echo "INFO - Starting core provisioning..."
sed -i 's/\r$//' $DIR/implement.sh
/usr/bin/env bash "$DIR/implement.sh"
echo "INFO - project provisioning completed successfully!"
SCRIPT

$nextcloud_provisioner = <<-SCRIPT
DIR="/vagrant/provision/bootstrap"
echo "INFO - Starting core provisioning..."
sed -i 's/\r$//' $DIR/nextcloud.sh
/usr/bin/env bash "$DIR/nextcloud.sh"
echo "INFO - project provisioning completed successfully!"
SCRIPT

$init_centos_provisioner = <<-SCRIPT
sudo sysctl -w net.ipv4.ip_forward=1
sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_centos    
sudo service sshd restart
sudo yum -y install dos2unix
sudo yum clean all
echo "nameserver 8.8.8.8" >> /etc/resolv.conf
SCRIPT

$bootstrap_centos_provisioner = <<-SCRIPT
DIR="/vagrant/provision/bootstrap"
echo "INFO - Starting OpenVPN provisioning..."
sed -i 's/\r$//' $DIR/openvpn.sh
sed -i 's/\r$//' $DIR/create-client.sh
/usr/bin/env bash "$DIR/openvpn.sh"
echo "INFO - OpenVPN provisioning completed successfully!"
SCRIPT
