# Workstation Setup

## Install VirtualBox
Please uninstall any old version of VirtualBox you may have used before and install v7.0.6: https://download.virtualbox.org/virtualbox/7.0.6/VirtualBox-7.0.6-155176-Win.exe 

## Install Vagrant
Please uninstall any old version of Vagrant you may have used before and install v2.3.4 (the latest version at the time of writing this guide): 
https://releases.hashicorp.com/vagrant/2.3.4/vagrant_2.3.4_windows_i686.msi

## Setup
Here are detailed steps of what you need to do: 
1. Type `vagrant plugin install vagrant-vbguest --plugin-version 0.21.0` and click enter (please use version 0.21.0 - the latest version does not work correctly). This will install the VirtualBox Guest Additions plugin for vagrant, which allows you to mount shared folders between your vagrant box and the host machine.

    OR

    3.2	Type `vagrant up` and click enter. Vagrant will then load and parse the Vagrantfile in this directory and create (and provision) your VM. This will take some time, so feel free to grab a coffee until the VM provisioning is complete ☕
4.	Add the following entry to your host machine's hosts file (located at C:\Windows\System32\drivers\etc\hosts or in MACOS /etc/hosts): project  
5. Get the SSH config that Vagrant uses: `vagrant ssh-config` and copy the output of this into an SSH config file at ~/.ssh/config. In VScode you can easily open this file, or generate a custom config file for VSCode to use, by pressing ⌘⇧P and selecting Remote-SSH: Open Configuration File…
