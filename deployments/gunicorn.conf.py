#server address
bind = ["0.0.0.0:8100"]
# bind = ["unix:/srv/project/core/app.sock"]

#workers definition
workers = 1
# worker_class="gevent"
# threads=100
timeout=300

# the access log file
# accesslog = '-' # TO SHOW IN TERMINAL and journald
# errorlog = '-' # TO SHOW IN TERMINAL and journald

accesslog = "/var/log/project/project-access.log" # '-' TO SHOW IN TERMINAL and journald
errorlog = "/var/log/project/project-error.log" # '-' TO SHOW IN TERMINAL and journald
loglevel = "debug" # heighest log level
capture_output = True

# load python code only once -> gunicorn has to be restarted for code changes to show
# preload_app = True

#sets dedicated environment variables
raw_env = []

def on_starting(server):
    # this should be called only once!
    # before the server is started

    # kill background capture service & restart it
    # connect the server to the capture service queue
    pass

def when_ready(server):
    # this should be called only once!
    # after the server is started
    pass

def post_fork(server, worker):
    # called before th worker starts
    pass
