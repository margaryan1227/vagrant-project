#!/usr/bin/env bash
echo "INFO - Deploying project..."
DIR="/srv/project/core" &&\
cd $DIR &&\
# /home/vagrant/.local/bin/gunicorn -c gunicorn.conf.py file_transver.wsgi:application &&\
# $(which gunicorn) -c gunicorn.conf.py file_transver.wsgi:application &&\
/usr/local/bin/python3.8 server.py &&\
echo "INFO - project deployment ended successfully."
